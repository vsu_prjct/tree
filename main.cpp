#include <iostream>
using namespace std;

struct Node{
	int data;// ��������
	Node *r, *l; // ��������� �� ����� ������
};

/* ����� ����� */
void show_left(Node *&Tree){
	if (Tree != NULL){
		show_left(Tree->l);
		cout << Tree->data; // ����� ��������
		show_left(Tree->r);
	}
}

/* ������ ����� */
void show_right(Node *&Tree){
	if (Tree != NULL){
		show_right(Tree->r);
		cout << Tree->data; // ����� ��������
		show_right(Tree->l);
	}
}

/* ������ ���� ����� */
void show_atop(Node *&Tree){
	if (Tree != NULL){
		cout << Tree->data; // ����� ��������
		show_right(Tree->l);
		show_right(Tree->r);
	}
}

/* ��������� ������ */
void show(Node *&Tree, int h){
	if (Tree != NULL){
		show(Tree->l, h + 2);
		for (int i = 0; i < h; i++){
			cout << ' ';
		}
		cout << Tree->data << endl;
		show(Tree->r, h + 2);
	}
}

/* �������� ����� */
void create(Node *&Tree){
	if (Tree == NULL){
		int x;
		cout << "������� ������: ";
		cin >> x;
		Tree = new Node;
		Tree->data = x;
		Tree->l = Tree->r = NULL;
	}
}

/* ���������� �������� */
void add_node(int x, Node *&newTree){
	// ����� ���������
	if (x < newTree->data){
		if (newTree->l != NULL) add_node(x, newTree->l);
		else{
			newTree->l = new Node;
			newTree->l->data = x;
			newTree->l->l = newTree->l->r = NULL;
		}
	}

	// ������ ���������
	if (x > newTree->data){
		if (newTree->r != NULL) add_node(x, newTree->r);
		else{
			newTree->r = new Node;
			newTree->r->data = x;
			newTree->r->l = newTree->r->r = NULL;
		}
	}
}

/* ������� ������� ���������� ����, ��� ������ */
void remove_transfer(int x, Node *parentTree, Node *childTree){
	if (parentTree->data < x) parentTree->r = childTree;
	else parentTree->l = childTree;
}

/* ������ �������� */
void remove_right(int x, Node *parentTree, Node *removeTree){
	Node *childremoveTree = removeTree->r;
	if (!childremoveTree->l){
		childremoveTree->l = removeTree->l;
		remove_transfer(x, parentTree, childremoveTree);

		// �������� ����
		removeTree = NULL;
		delete removeTree;
	}
	else{
		Node *parentChildremoveTree = NULL;
		// ����� ����. �� ������ �����
		while (childremoveTree->l){
			parentChildremoveTree = childremoveTree;
			childremoveTree = childremoveTree->l;
		}
		removeTree->data = childremoveTree->data; // ������ ��������, �������� �����

		// ������� ��������
		if (!childremoveTree->r) parentChildremoveTree->l = NULL;
		else parentChildremoveTree->l = childremoveTree->r;

		delete childremoveTree;
	}
}

/* �������� ���� */
bool remove(int x, Node *Tree){
	if (!Tree || (!Tree->l && !Tree->r) || Tree->data == x) return false;

	Node *parentTree = NULL; // ������ ���������� ��������

	/* ���� ������� � ������ */
	while (Tree){
		if (Tree->data > x){
			parentTree = Tree;
			Tree = Tree->l;
		}
		else if (Tree->data < x){
			parentTree = Tree;
			Tree = Tree->r;
		}
		else break;
	}
	if (!Tree) return false;

	/* ������� �������� */
	if (Tree->l && Tree->r) remove_right(x, parentTree, Tree);
	else{
		if (!Tree->r) remove_transfer(x, parentTree, Tree->l);
		else if (!Tree->l) remove_transfer(x, parentTree, Tree->r);
		else remove_transfer(x, parentTree, NULL);
		// �������� ����
		Tree = NULL;
		delete Tree;
	}

	return true;
}

/* �������� ���� (����� �������) */
void remove_el(int x, Node *Tree)
{
	if (!Tree || (!Tree->l && !Tree->r) || Tree->data == x) return;

	Node *parentTree = NULL; // ������ ���������� ��������
	/* ���� ������� � ������ */
	while (Tree){
		if (Tree->data > x){
			parentTree = Tree;
			Tree = Tree->l;
		}
		else if (Tree->data < x){
			parentTree = Tree;
			Tree = Tree->r;
		}
		else break;
	}
	if (!Tree) return;

	/* �������� */
	// ������� ��� ��������
	if (!Tree->l && !Tree->r){
		if (parentTree->data < x) parentTree->r = NULL;
		else parentTree->l = NULL;
		Tree = NULL;
		delete Tree;
	}
	else if (Tree->l && !Tree->r){
		if (parentTree->data < x) parentTree->r = Tree->l;
		else parentTree->l = Tree->l;
		Tree = NULL;
		delete Tree;
	}
	else if (!Tree->l && Tree->r){
		if (parentTree->data < x) parentTree->r = Tree->r;
		else parentTree->l = Tree->r;
		Tree = NULL;
		delete Tree;
	}
	else if (Tree->l && Tree->r){
		/* ������ �������� */
		Node *rtree = Tree->r;
		if (!rtree->l){
			if (parentTree->data < x) parentTree->r = rtree;
			else parentTree->l = rtree;
			rtree->l = Tree->l;
			Tree->r = Tree->l = NULL;
			delete Tree;
		}
		else{
			Node *parentRtree = NULL;
			while (rtree->l){
				parentRtree = rtree;
				rtree = rtree->l;
			}
			Tree->data = rtree->data;

			if (!rtree->r) parentRtree->l = NULL;
			else parentRtree->l = rtree->r;

			delete rtree;
		}
	}
}

/* ������� ����� � ������ */
int count_node(Node *Tree){
	if (Tree == NULL) return 0;
	return 1 + count_node(Tree->l) + count_node(Tree->r);
}

/* ����� ����� � �������� */
void find_node(Node *Tree, int *arr, int &n){
	if (Tree != NULL){
		find_node(Tree->l, arr, n);
		if (abs(count_node(Tree->l) - count_node(Tree->r)) == 1){
			arr[n] = Tree->data;
			n++;
		}
		find_node(Tree->r, arr, n);
	}
}

int main(){
	setlocale(0, "");

	short int n; // ���-�� �����
	do{
		cout << "������� ���-�� ����� ������: ";
		cin >> n;
	} while (n < 1);

	/* �������� ������ */
	Node *tree = NULL;
	create(tree);

	/* ���������� ������ */
	int x;
	for (int i = 1; i <= n - 1; i++){
		cout << "������� " << i << " ���� ������: ";
		cin >> x;
		add_node(x, tree);
	}
	cout << "���������� ������" << endl;
	show(tree, 2);
	cout << endl;

	/* �������� ���� */
	int delX;
	cout << "����� ���� �������: ";
	cin >> delX;
	cout << endl;
	if (remove(delX, tree)){
		cout << " *** ���� �����! *** \n ���������� ������ ������" << endl;
		show(tree, 2);
	}
	else cout << " *** ������! ���� �� ������. *** ";
	
	cin.get();
	cin.get();
	return 0;
}

